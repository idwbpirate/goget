package main

import (
	"database/sql"
    "fmt"
    "log"
    "net/http"
	_ "github.com/lib/pq"
)

func ping(w http.ResponseWriter, r *http.Request) {

	// Open db
	db, err := sql.Open("postgres", "host=localhost port=5432 user=test password=test dbname=db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rows, err := db.Query(
		`SELECT environment_uuid, environment, port FROM public.where_am_i()`)
	if err != nil {
		log.Fatal(err)
	}

	// Rows
	for rows.Next() {
		var environment_uuid string
		var environment string
		var port string
		err = rows.Scan(&environment_uuid, &environment, &port)
		checkErr(err)
		fmt.Fprintln(w, "environment_uuid | environment | port\n")
		fmt.Fprintf(w, "%8v | %8v | %8v \n", environment_uuid, environment, port)
	}
}

func pagehandler(w http.ResponseWriter, r *http.Request) {

	fmt.Fprintln(w, "GoLang simple page.\n")

}

func main() {
    http.HandleFunc("/page", pagehandler)
    http.HandleFunc("/ping", ping)
    log.Fatal(http.ListenAndServe(":8080", nil))
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
